FROM python:3.8-slim

WORKDIR /usr/src/app

ADD app.py /usr/src/app/
ADD newrelic.ini /usr/src/app/newrelic.ini
ADD example_data.py /usr/src/app/
ADD requirements.txt /usr/src/app/

RUN pip3 install -r requirements.txt

ENV FLASK_APP /usr/src/app/app.py
ENV NEW_RELIC_CONFIG_FILE /usr/src/app/newrelic.ini

CMD ["newrelic-admin", "run-program", "flask", "run", "--host=0.0.0.0"]

EXPOSE 5000
